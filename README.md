# Ark Mod Server Monitoring NodeJS tool

This is a basic NodeJS / DB server monitoring tool built specifically for Ark Mods to be able to stop servers from loading the mod if they break ToS. 
Created by the developers of Primal Fear.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
```
This was tested on Ubuntu but any OS will do if you can install the pre-reqs yourself.

Postgres SQL
NodeJS
pgAdmin4
Ark Dev Kit
```

### Installing

A step by step series of examples that tell you how to get a development env running

Install Postgres SQL

```
apt-get install postgres-sql
```
Install NodeJS
```
apt-get install nodejs

```
Clone this repo 
```

git clone git@bitbucket.org:ASimpson_/ark_server_monitor_node.git

```
DB Manager
```

You can manage the DB through the linux cli but i like to use pgadmin - https://www.pgadmin.org/download/

```
Setup a DB with pgadmin4 

```
For Your DB to work out of the box with this NodeJS setup a db and change the DB details in /bin/www , create a table called serverlist and 2 columns ( serverip, banned )

```
Final setup

```
Go to the root directory of this repo and use your favourite text editor to open /bin/www. Edit the DB URL. Save and Close.

Finally run 

nodejs ./bin/www 

you should now see login server listening at http://:::3000

If you want to add/modify functions simply open up index.js and go ham.

```

## BLUEPRINTS 
For use in the ADK. This requires some completion to the success or banned functions after the switch.

https://blueprintue.com/blueprint/zti6lfg4/

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
