var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json({ type: 'application/json' }));
var postgres = require('./lib/postgres');
const basicAuth = require('express-basic-auth')
 var loginRouter = express.Router();

function doesExistServer(req, res, next) {
var ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress
var stripped = ip.replace(/^.*:/, '')
    var queryCheck = "SELECT EXISTS(select 1 from serverlist where serverip = $1)";
  postgres.client.query(queryCheck, [stripped], function checkExists(err, result) {
        if (err) {
      console.error(err);
      res.statusCode = 500;
      return res.json({ errors: ['failed to check for existing'] });
    }
    res.login = result.rows[0];
    next();
  });
}

function insertNewServer(req, res) {
var ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress
var stripped = ip.replace(/^.*:/, '')
  var sql = "INSERT INTO serverlist (serverip, banned) VALUES ($1, $2) RETURNING serverip";
  var data = [
    stripped,
    false
  ];

  postgres.client.query(sql, data, function InsertServer(err, result) {
    if (err) {
      console.error(err);
      res.statusCode = 500;
      return res.json({
        errors: ["Error code " +  res.statusCode + ' Failed to add server' ]
      });
    }
    res.json({ Success: ["Added server succesfully"] });
  });
}

function checkifbanned(req, res) {
var ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress
var stripped = ip.replace(/^.*:/, '')
  console.log('server registration attempt', stripped);

  var queryCheck = "SELECT * FROM serverlist where serverip = $1";

  postgres.client.query(queryCheck, [stripped], function checkExists(err, result) {
        if (err) {
      console.error(err);
      res.statusCode = 500;
      return res.json({ errors: ['failed to check if banned'] });
    }
    if(result.rows[0] !== undefined) {
          if(result.rows[0].banned === true) {
        res.json({ result: ['banned'] });
      }else {
        res.json({ result: ['success'] });
      }
    }
  });
}


app.use('/login', loginRouter);

module.exports = app;